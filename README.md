# AWT - Adventure in Wahi Teboka

Notre jeu est de type Rogue-Like. De ce fait, il vous faudra vous y prendre à
plusieurs fois avant de vous imprégner de ce style de jeu.

Votre objectif est simple : avancer de salle en salle en éliminant un maximum
d'ennemis sur votre route pour enfin accéder à la salle du boss puis l'éliminer
pour arriver jusqu'à la salle finale. En effet, votre route sera remplie
d'embûches mais l'ajout d'une arme et de compétences rendra votre voyage bien
plus attractif !

By group TETA.